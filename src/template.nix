{ fetchgit, lib, rustc, stdenv }:
let
  indexOfInner = n: x: xs: if n < builtins.length xs
                           then if builtins.elemAt xs n == x
                                then n
                                else indexOfInner (n+1) x xs
                           else null;
  indexOf = indexOfInner 0;

  COMPILE = "debug";
in
rec {
  mkCrate = key: data: stdenv.mkDerivation rec {
    name = key;
    src = data.src;
    doConfigure = false;
    buildInputs = map (s: crates."${s}") data.deps ++ (if crates ? "${key}-build" then [ crates."${key}-build" ] else []);
    buildPhase = let
        srcPath = if data.type == "build-script" then "." else "src";
        args = map (arg:
          builtins.replaceStrings ["@@target_dir@@" "@@source_dir@@"] ["target" srcPath] (lib.escapeShellArg arg)
        ) data.args;
        outIndex = indexOf (lib.escapeShellArg "--out-dir") args;
        outDir = if outIndex == null
                 then ""
                 else "${builtins.elemAt args (outIndex + 1)}";
        join = builtins.concatStringsSep "\n";

        bannedEnvs = ["LD_LIBRARY_PATH" "PATH"];
        envs = builtins.filter (p: !(builtins.elem p bannedEnvs)) (builtins.attrNames data.env);
      in ''
      mkdir -p target/${COMPILE}/deps
      mkdir -p target/${COMPILE}/build
      ${join (
        map (dep: ''
          cp -rf "${dep}/deps/"* target/${COMPILE}/deps/ 2> /dev/null || true
          cp -rf "${dep}/build/"* target/${COMPILE}/build/ 2> /dev/null || true
        '') buildInputs
      )}
      ${if outDir == null
        then ""
        else "mkdir -p ${outDir}"}
      ${join (
        map (key: ''
          export ${lib.escapeShellArg key}=${lib.escapeShellArg data.env."${key}"}
        '') envs
      )}
      echo $'\x1b'[36m Running: rustc ${toString args} $'\x1b'[m
      ${rustc}/bin/rustc ${toString args}
      ${if data.type != "build-script"
        then ""
        else ''
          # TODO: Handle build scripts
          # find ${outDir} -executable -type f -exec {} \;
          # ls -R target/${COMPILE}
        ''}
    '';
    installPhase = ''
      # Install binaries
      mkdir -p "$out/bin"
      find target/${COMPILE} -type f \
        -executable \
        -maxdepth 1 \
        -exec cp "{}" "$out/bin" \;

      # Install built crates that might be needed
      cp -r target/${COMPILE}/deps "$out/deps" 2> /dev/null || true

      # Install other important artifacts
      cp -r target/${COMPILE}/build "$out/build" 2> /dev/null || true
    '';
  };
  fetchRustCrate = builtins.fetchTarball;

  crates = builtins.listToAttrs (map (key: {
    name = key;
    value = mkCrate key cratesData."${key}";
  }) (builtins.attrNames cratesData));

  cratesData = {insert-crates=here;  };
}
