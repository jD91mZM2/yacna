// Not `use clap::macro` because the macro I'm after uses more macros
// behind the scenes.
#[macro_use]
extern crate clap;

use cargo::{
    core::{
        compiler::{Context, CompileMode, DefaultExecutor, Executor, Kind, TargetInfo, Unit},
        manifest::Target,
        package_id::PackageId,
        Workspace
    },
    ops::{self, CompileOptions, CleanOptions},
    util::{
        config::Config,
        errors::CargoResult,
        important_paths::find_root_manifest_for_wd,
        process_builder::ProcessBuilder
    }
};
use clap::Arg;
use failure::format_err;
use serde_derive::Deserialize;
use std::{
    collections::HashMap,
    env,
    error::Error,
    ffi::OsStr,
    fmt::Write as _,
    fs,
    io::{self, Write as _},
    path::PathBuf,
    process::Command,
    sync::Arc
};

const RELEASE: bool = false;

const LINE_WIDTH: usize = 80;
const INDENTION_UNITS: usize = 2;
const INDENTION: usize = 2;

fn main() -> Result<(), Box<dyn Error>> {
    let args = app_from_crate!()
        .arg(Arg::with_name("manifest-path")
             .long("manifest-path")
             .takes_value(true)
             .help("Specifes the path to Cargo.toml"))
        .arg(Arg::with_name("target")
             .long("target")
             .takes_value(true)
             .help("Specify the target triple. Defaults to the host target."))
        .get_matches();

    let target = args.value_of("target").map(String::from);

    let cwd = env::current_dir()?;
    let manifest = match args.value_of("manifest-path") {
        Some(path) => fs::canonicalize(path)?,
        None => find_root_manifest_for_wd(&cwd)?
    };

    let mut config = Config::default()?;
    config.configure(
        0,
        Some(false),
        &None,
        false,
        false,
        false,
        &None,
        &[]
    )?;

    // Clean
    let workspace = Workspace::new(&manifest, &config)?;

    ops::clean(&workspace, &CleanOptions {
        config: &config,
        spec: Vec::new(),
        target: None,
        release: RELEASE,
        doc: false
    })?;

    let mut options = CompileOptions::new(&config, CompileMode::Build)?;
    options.build_config.release = RELEASE;

    let rustc = config.load_global_rustc(Some(&workspace))?;
    let target_or_host = target.clone().unwrap_or(rustc.host.clone());
    let targetinfo = TargetInfo::new(
        &config,
        &target,
        &rustc,
        if target.is_none() { Kind::Target } else { Kind::Host }
    )?;

    // Build a map of all resolved dependencies
    let (_packages, resolve) = ops::resolve_ws_precisely(
        &workspace,
        &options.features,
        options.all_features,
        options.no_default_features,
        &options.spec.to_package_id_specs(&workspace)?
    )?;

    let mut todo: Vec<_> = workspace.members().map(|m| m.package_id()).collect();
    let mut resolved = HashMap::new();
    while let Some(package) = todo.pop() {
        let deps: Vec<_> = resolve.deps(package)
            .filter(|&(_, deps)| deps.iter().any(|dep| {
                dep.platform().map(|platform| platform.matches(&target_or_host, targetinfo.cfg())).unwrap_or(true)
            }))
            .map(|(dep_id, _)| dep_id)
            .collect();
        todo.extend_from_slice(&deps);
        resolved.insert(package, deps);
    }

    // Execute and output
    let executor: Arc<dyn Executor> = Arc::new(MyExecutor {
        inner: DefaultExecutor,
        root: PathBuf::from(workspace.root()),
        target_dir: PathBuf::from(workspace.target_dir().as_path_unlocked()),
        dependencies: resolved
    });

    let mut parts = include_str!("template.nix").splitn(2, "insert-crates=here;");
    let first = parts.next().expect("invalid template: no first section");
        // .replace("insert-root-crates-here", "yee");
    let second = parts.next().expect("invalid template: no second section");

    println!("{}", first);
    ops::compile_with_exec(&workspace, &options, &executor)?;
    println!("{}", second);
    Ok(())
}

fn nix_escape(s: &str) -> String {
    // Crappy, but Good Enough:tm:
    s.replace('\\', r#"\\"#)
        .replace('"', r#"\""#)
        // .replace('$', r#"\$"#)
}

#[derive(Deserialize)]
struct GitPrefetch {
    url: String,
    rev: String,
    sha256: String,
}

fn download_for(p: PackageId, indent: usize) -> CargoResult<String> {
    let s = p.source_id();
    if s.is_default_registry() {
        Ok(format!("fetchRustCrate \"https://crates.io/api/v1/crates/{}/{}/download\"", p.name(), p.version()))
    } else if s.is_path() {
        let mut string = s.url().to_string();
        if string.starts_with("file://") {
            string.drain(..7);
        }
        Ok(string)
    } else if s.is_git() {
        let output = Command::new("nix-prefetch-git")
            .arg("--url")
            .arg(s.url().as_str())
            .arg("--rev")
            .arg(s.precise().expect("I thought 'precise' should always be set here, my bad! Report this."))
            .output()?;
        let prefetch: GitPrefetch = serde_json::from_slice(&output.stdout)?;
        Ok(format!(
            "fetchgit {{
{:indent1$}url = \"{}\";
{:indent1$}rev = \"{}\";
{:indent1$}sha256 = \"{}\";
{:indent0$}}}",
            "",
            nix_escape(&prefetch.url),
            "",
            nix_escape(&prefetch.rev),
            "",
            nix_escape(&prefetch.sha256),
            "",
            indent0 = indent,
            indent1 = indent + INDENTION_UNITS,
        ))
    } else {
        eprintln!("Warning: Support for this type of crate is not yet implemented, will use URL: {}", s);
        Ok(format!("fetchRustCrate \"{}\"", s.url().to_string()))
    }
}

fn package_key(p: PackageId) -> String {
    nix_escape(&format!("{}-{}", p.name().as_str(), p.version()))
}

struct MyExecutor {
    inner: DefaultExecutor,
    root: PathBuf,
    target_dir: PathBuf,
    dependencies: HashMap<PackageId, Vec<PackageId>>
}

impl Executor for MyExecutor {
    fn exec(
        &self,
        cmd: ProcessBuilder,
        id: PackageId,
        target: &Target,
        mode: CompileMode,
        on_stdout_line: &mut dyn FnMut(&str) -> CargoResult<()>,
        on_stderr_line: &mut dyn FnMut(&str) -> CargoResult<()>,
    ) -> CargoResult<()> {
        let mut indent = INDENTION * INDENTION_UNITS;

        let target_dir = self.target_dir.to_str()
            .ok_or_else(|| format_err!("path to target directory is not UTF-8"))?;
        let main = target.src_path().path()
            .ok_or_else(|| format_err!("One source path was not a path. I don't know what this means."))?;

        let mut my_key = package_key(id);
        if target.is_custom_build() {
            my_key.push_str("-build");
        }

        let mut out = String::new();

        writeln!(out, "{:indent$}\"{}\" = {{", "", my_key, indent=indent).unwrap();
        indent += INDENTION_UNITS;

        writeln!(out, "{:indent$}name = \"{}\";", "", nix_escape(id.name().as_str()), indent=indent).unwrap();
        writeln!(out, "{:indent$}version = \"{}\";", "", nix_escape(&id.version().to_string()), indent=indent).unwrap();
        writeln!(out, "{:indent$}src = {};", "", download_for(id, indent)?, indent=indent).unwrap();
        writeln!(out, "{:indent$}type = \"{}\";", "", target.kind().description(), indent=indent).unwrap();
        write!(out, "{:indent$}deps = [", "", indent=indent).unwrap();
        let mut last_newline = out.len() - 1;
        if let Some(deps) = self.dependencies.get(&id) {
            for &dep in deps {
                if out.len() - last_newline >= LINE_WIDTH {
                    last_newline = out.len() - 1;
                    write!(out, "\n{:indent$}", "", indent=(indent + 8)).unwrap();
                }
                write!(out, " \"{}\"", package_key(dep)).unwrap();
            }
        }
        out.push_str(" ];\n");

        last_newline = out.len() - 1;
        write!(out, "{:indent$}args = [", "", indent=indent).unwrap();
        for arg in cmd.get_args() {
            let arg = arg.to_str().ok_or_else(|| format_err!("path is not UTF-8: {}", arg.to_string_lossy()))?;
            let arg = if self.root.join(arg) == main {
                // We can unwrap() here as we already know "arg" is valid UTF-8
                format!("@@source_dir@@/{}", main.file_name().and_then(OsStr::to_str).unwrap())
            } else {
                // Once again, crappy, but Good Enough:tm:
                // Seems to be a recurring theme, doesn't it?
                arg.replace(target_dir, "@@target_dir@@")
            };

            if out.len() - last_newline >= LINE_WIDTH {
                last_newline = out.len() - 1;
                write!(out, "\n{:indent$}", "", indent=(indent + 8)).unwrap();
            }

            write!(out, " \"{}\"", nix_escape(&arg)).unwrap();
        }
        out.push_str(" ];\n");

        writeln!(out, "{:indent$}env = {{", "", indent=indent).unwrap();
        indent += INDENTION_UNITS;
        for (key, val) in cmd.get_envs() {
            if let Some(val) = val.as_ref().and_then(|s| s.to_str()) {
                let val = val.replace(target_dir, "@@target_dir@@");
                writeln!(out, "{:indent$}\"{}\" = \"{}\";", "", key, val, indent=indent).unwrap();
            }
        }
        indent -= INDENTION_UNITS;
        writeln!(out, "{:indent$}}};", "", indent=indent).unwrap();

        indent -= INDENTION_UNITS;
        writeln!(out, "{:indent$}}};", "", indent=indent).unwrap();

        io::stdout().lock().write_all(out.as_bytes())?;

        self.inner.exec(cmd, id, target, mode, on_stdout_line, on_stderr_line)
    }
    fn init<'a, 'cfg>(&self, cx: &Context<'a, 'cfg>, unit: &Unit<'a>) {
        self.inner.init(cx, unit)
    }
    fn force_rebuild(&self, unit: &Unit) -> bool {
        self.inner.force_rebuild(unit)
    }
}
